angular.module('musixApp')
.factory('musicService', [function(){
  return [
    {
      nombre: 'cancion',
      url: '/res/music/song_1.mp3',
      album: { nombre: 'album1', image: '/res/album/album1.jpg'},
      artista: 'artista 1'
    },
    {
      nombre: 'cancion',
      url: '/res/music/song_2.mp3',
      album: { nombre: 'album1', image: '/res/album/album1.jpg'},
      artista: 'artista 1'
    },
    {
      nombre: 'cancion',
      url: '/res/music/song_3.mp3',
      album: { nombre: 'album1', image: '/res/album/album1.jpg'},
      artista: 'artista 1'
    },
    {
      nombre: 'cancion',
      url: '/res/music/song_4.mp3',
      album: { nombre: 'album1', image: '/res/album/album1.jpg'},
      artista: 'artista 1'
    },
    {
      nombre: 'cancion',
      url: '/res/music/song_5.mp3',
      album: { nombre: 'album1', image: '/res/album/album1.jpg'},
      artista: 'artista 1'
    },
    {
      nombre: 'cancion',
      url: '/res/music/song_6.mp3',
      album: { nombre: 'album1', image: '/res/album/album1.jpg'},
      artista: 'artista 1'
    },
  ]
}]);