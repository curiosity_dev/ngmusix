angular.module('musixApp')
.controller('musicCtrl', ['$scope', 'musicService', 
  function($scope, musicService){
    console.log('music controller here!');
    $scope.init = function(){
      $scope.musics = musicService;
      console.log($scope.musics)
    }
  }
])
.controller('reproductorCtrl', ['$scope', '$interval', 'musicService', 
  function($scope, $interval, musicService){
    var my_audio = document.getElementById("audio");
    $scope.load = function(){
      $scope.is_playing = false;
      $scope.volume = 100;
      $scope.buffer = 0;
    };
    $scope.play = function(){
      
      my_audio.play();
      $scope.is_playing = true;
    };
    $scope.pause = function(){
      my_audio.pause();
      $scope.is_playing = false;
    };

    $scope.anterior = function(){
      my_audio.pause();
      my_audio.src = '/res/music/song_1.mp3';
      my_audio.load();
      my_audio.play();
    };
    $scope.siguiente = function(){
      my_audio.pause();
      my_audio.src = '/res/music/song_21.mp3';
      my_audio.load();
      my_audio.play();
    };
    $scope.$watch('volume', function(ant, nue) {
      var volumen = nue / 100.0;
      var my_audio = document.getElementById("audio");
      my_audio.volume = volumen;
      console.log(nue);
    });

    var stop;
    stop = $interval(function() {
      if($scope.is_playing)
        $scope.buffer = (my_audio.currentTime / my_audio.duration)*100;
      else 
        $scope.buffer = 0;
    }, 1000);
  }
]);